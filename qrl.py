#!/usr/bin/env python3
from flask import Flask, abort, send_file, render_template
import flask
from flask_cors import CORS
from flask import request
from pprint import pprint as pp
import json
import qrcode
import socket
import os

app = Flask('app')
CORS(app)

DATA_FILENAME = "data.json"

@app.route('/get')
def get():
    with open(DATA_FILENAME, "r") as f:
        return f.read()

@app.route('/post', methods=["POST"])
def post():
    # get current data from file
    with open(DATA_FILENAME, "r") as f:
        data = json.load(f)
        print(data)

    print(request.json)

    # add new data
    data.append(request.json)

    # write all data
    with open(DATA_FILENAME, "w") as f:
        json.dump(data, f, indent=4)

    # return the all data - including the new one
    return json.dumps(data)

@app.route('/', defaults={'req_path': ''})
@app.route('/<path:req_path>')
def dir_listing(req_path):
    BASE_DIR = os.getcwd()

    print(f"BASE_DIR: {BASE_DIR}")

    # Joining the base and the requested path
    abs_path = os.path.join(BASE_DIR, req_path)

    print(f"abs_path: {abs_path}")
    # Return 404 if path doesn't exist
    if not os.path.exists(abs_path):
        return abort(404)

    # Check if path is a file and serve
    if os.path.isfile(abs_path):
        return send_file(abs_path)

    # Show directory contents
    files = os.listdir(abs_path)
    return render_template('files.html', files=files)

host_name = socket.gethostname() 
host_ip = socket.gethostbyname(f"{host_name}.local") 

print(f"IP: {host_ip}\nHostname:{host_name}")

qr = qrcode.QRCode()
link = f"http://{host_ip}:8080/"
qr.add_data(link)

qr.print_ascii()
print(link)

app.run(host='0.0.0.0', port=8080)